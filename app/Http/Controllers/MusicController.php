<?php

namespace App\Http\Controllers;

use App\Models\Music;
use Illuminate\Http\Request;

class MusicController extends Controller
{
    // ALL MUSICS
    public function index()
    {
        $musics = Music::all();
        return response()->json($musics);
    }

    // CREATE NEW MUSIC
    public function store(Request $request)
    {
       // $validateData = $request->validate([
       //      'title' => 'required|string',
       //      'artist' => 'required|string',
       //      'album' => '',
       //      'gender' => '',
       //      'description' => 'required|string',
       //      'location' => 'required|string'
       // ]);
       $music = Music::create($request->all());
       return response()->json($music, 201);
    }

    // GET ONE MUSIC BY ID
    public function show(Music $music)
    {
        return response()->json($music);
    }

    // UPDATE MUSIC
    public function update(Request $request, Music $music)
    {
        $validateData = $request->validate([
            'title' => 'required|string',
            'artist' => 'required|string',
            'album' => '',
            'gender' => '',
            'description' => 'required|string',
            'location' => 'required|string'
       ]);
       $music->update($validateData);
       return response()->json($music);
    }

    // DELETE MUSIC
    public function destroy(Music $music)
    {
        $music->delete();
        return response()->json(null, 204);
    }
}