<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MusicController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/musics', [MusicController::class, 'index']);
Route::post('/musics', [MusicController::class, 'store']);
Route::get('/musics/{music}', [MusicController::class, 'show']);
Route::put('/musics/{music}', [MusicController::class, 'update']);
Route::delete('/musics/{music}', [MusicController::class, 'destroy']);